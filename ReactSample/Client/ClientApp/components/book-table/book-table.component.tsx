import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import * as BookStore from '../../store/books.store';
import './book-table.component.less';
import { ApplicationState } from 'ClientApp/configureStore';
// At runtime, Redux will merge together...
type BooksTableProps = BookStore.BooksState 
& typeof BookStore.actionCreators 
& RouteComponentProps<{}>
& {
    authorId:string;
};


class BooksTableComponent extends React.Component<BooksTableProps, {onBookClick:()=>void}> {
	componentWillMount() {
		// This method runs when the component is first added to the page
        this.props.requestBooks(this.props.authorId);
	}

	componentWillReceiveProps(nextProps: BooksTableProps) {
        if(this.props.authorId != nextProps.authorId){
            this.props.requestBooks(nextProps.authorId);
        }
    }

	public render() {
		return <table className='table book-table'>
        <thead>
            <tr>
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
            {this.props.books.map(book =>
                <tr key={book._id}>
                    <td>{book.title}</td>
                </tr>
            )}
        </tbody>
    </table>;
	}
}

export default connect(
	(state: ApplicationState) => state.book,
     BookStore.actionCreators
)(BooksTableComponent as any) as typeof BooksTableComponent;

