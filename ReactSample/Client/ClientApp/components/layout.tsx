import * as React from 'react';
import { NavLink, Link } from 'react-router-dom';
export class Layout extends React.Component<{}, { fullname: string; }> {
    componentWillMount() {
        var userJson: any = localStorage.getItem('user');
        if (!userJson) {
            return;
        }
        var user:any = JSON.parse(userJson);
        if (!user) {
            return;
        }
        this.setState({
            fullname: user.fullName
        })
    }

    public render() {
        return <div className='container-fluid'>
            <div className='row'>
                <div className='col-sm-3'>
                    <div className='main-nav'>
                        <div className='navbar navbar-inverse'>
                            <div className='navbar-header'>
                                <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                                    <span className='sr-only'>Toggle navigation</span>
                                    <span className='icon-bar'></span>
                                    <span className='icon-bar'></span>
                                    <span className='icon-bar'></span>
                                </button>
                                <Link className='navbar-brand' to={'/'}>
                                    {this.state.fullname}
                                </Link>
                            </div>
                            <div className='clearfix'></div>
                            <div className='navbar-collapse collapse'>
                                <ul className='nav navbar-nav'>
                                    <li>
                                        <NavLink exact to={'/'} activeClassName='active'>
                                            <span className='glyphicon glyphicon-home'></span> Home
                            </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to={'/books'} activeClassName='active'>
                                            <span className='glyphicon glyphicon-book'></span> Books
                            </NavLink>
                                    </li>
                                    <li>
                                        <NavLink onClick={() => { localStorage.clear(); window.location.reload(true) }} exact to={'/'} activeClassName='active1'>
                                            <span className='glyphicon glyphicon-off'></span> Log Out
                            </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col-sm-9'>
                    {this.props.children}
                </div>
            </div>
        </div>;
    }
}