import { BaseService } from "./base.service";
import * as models from "../../../shared/models"
import axios, { AxiosPromise } from "axios"
import { environment } from "../environment/environment"
import { IUserModel } from "../../../shared/models";
export class AuthService extends BaseService {
    constructor() {
        super();

    }

    isAuthorized = () => {
        var token = localStorage.getItem('token');
        if (!token) {
            return false;
        }
        return true;
    }

    login = (username: string, password: string): AxiosPromise<{ token: string, user: IUserModel }> => {
        return axios.post(environment().apiEndpoint + "auth/login", {
            login: username,
            password: password
        });
    }

    register = (model: { email: string, password: string, fullName: string }): AxiosPromise<void> => {
        return axios.post(environment().apiEndpoint + "auth/register", model)
    }

    getProfile = (): AxiosPromise<models.IUserModel> => {
        return axios.get(environment().apiEndpoint + "auth/profile");
    }
}