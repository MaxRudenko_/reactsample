import { Action, Reducer, Dispatch } from 'redux';
import { AuthService } from "../../services"
import { put, call } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga/effects';
import { IUserViewModel } from '../../../../shared/viewModels';
var authService = new AuthService();

export interface LoginState {
    isLoading: boolean;
    token: string;
    user: IUserViewModel;
    message?: string;
}
interface LoginActionRequest {
    type: "LOGIN_AUTH_REQUEST";
    username: string;
    password: string;
}
interface LoginActionInit {
    type: "LOGIN_AUTH_INIT";
}
interface LoginActionSuccess {
    type: "LOGIN_AUTH_SUCCESS";
    token: string;
    user: IUserViewModel;
}
interface LoginActionFailure {
    type: "LOGIN_AUTH_FAILURE";
    message: string;
}
type KnownAction = LoginActionRequest | LoginActionInit | LoginActionSuccess | LoginActionFailure;
export interface DispatchProps {
    login(email: string, password: string): void;
}
export const DispatchMapper = (dispatch: Dispatch<KnownAction>): DispatchProps => {
    return {
        login: (email: string, password: string) => {
            dispatch({ type: "LOGIN_AUTH_REQUEST", email: email, password: password })
        }
    };
};
const unloadedState: LoginState = {
    isLoading: false, token: "", user: {
        _id: "",
        fullName: "",
        email: "",
        password: ""
    },
    message: undefined
};

export const reducer: Reducer<LoginState> = (state: LoginState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case "LOGIN_AUTH_INIT":
            state.isLoading = true;
            state.message = undefined;
            return state;
        case "LOGIN_AUTH_SUCCESS":
            return {
                token: action.token,
                isLoading: false,
                user: action.user,
                message: undefined
            };
        case "LOGIN_AUTH_FAILURE":
            return {
                message: action.message,
                isLoading: false, token: "", user: {
                    _id: "",
                    fullName: "",
                    email: "",
                    password: ""
                }
            };
    }
    return state || unloadedState;
};
function* loginSaga() {
    yield takeEvery("LOGIN_AUTH_REQUEST", function* (action: LoginActionRequest) {
        try {
            yield put({ type: "LOGIN_AUTH_INIT" });
            const { response, error } = yield call(authService.login, action.username, action.password);
            if (response) {
                yield put({ type: "LOGIN_AUTH_SUCCESS", token: response.data.token, user: response.data.user });
            }
            else {
                yield put({ type: 'LOGIN_AUTH_FAILURE', error })
            }

        } catch (e) {
            console.log(e);
            yield put({ type: "LOGIN_AUTH_FAILURE", message: e.message });
        }
    });
}
export const loginSagaList: any[] = [loginSaga()]
