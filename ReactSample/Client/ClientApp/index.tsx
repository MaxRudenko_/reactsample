import "bootstrap/dist/css/bootstrap.min.css";
import './less/site.less';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import configureStore from './configureStore';
import * as RoutesModule from './routes';
import { AuthService } from "./services";
let routes = RoutesModule.routes;

let unauthorizedRoutes = RoutesModule.unauthorizedRoutes;
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href')!;
const history = createBrowserHistory({ basename: baseUrl });
const store = configureStore(history);
var authService = new AuthService();


function renderApp() {
    ReactDOM.render(
        // <AppContainer>
            <Provider store={store}>
                {authService.isAuthorized() ?
                    <ConnectedRouter history={history} children={routes} /> :
                    <ConnectedRouter history={history} children={unauthorizedRoutes} />}
            </Provider>
        // </AppContainer>,
        document.getElementById('react-app')
    );
}
renderApp();
// Allow Hot Module Replacement
if (module.hot) {
    module.hot.accept('./routes', () => {
        routes = require('./routes').routes;
        renderApp();
    });
}