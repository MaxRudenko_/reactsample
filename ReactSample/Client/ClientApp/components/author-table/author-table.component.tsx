import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import * as AuthorStore from '../../store/authors.store';
import './author-table.component.less';
import { ApplicationState } from 'ClientApp/configureStore';


type DispatchProps = typeof AuthorStore.actionCreators;

interface StateProps extends AuthorStore.AuthorState {
    onAuthorClick?(id: string): void;
}

interface Props extends RouteComponentProps<{ authorId: string }>, StateProps, DispatchProps { }

class AuthorsTableComponent extends React.Component<Props, {}> {
    componentWillMount() {
        // This method runs when the component is first added to the page
        this.props.requestAuthors();
    }

    componentWillReceiveProps(nextProps: Props) {
    }

    public selectAuthor(id: string) {
        if (!this.props.onAuthorClick) {
            return;
        }
        this.props.onAuthorClick(id);
    }

    public render() {
        return <table className='table author-table'>
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {this.props.authors.map(author =>
                    <tr key={author._id} onClick={e => this.selectAuthor(author._id as string)}>
                        <td>{author.name}</td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

export default connect(
    (state: ApplicationState) => state.author,
    AuthorStore.actionCreators
)(AuthorsTableComponent as any) as typeof AuthorsTableComponent;

