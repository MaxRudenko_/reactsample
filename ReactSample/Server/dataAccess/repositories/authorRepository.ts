import * as mongoose from '../../node_modules/mongoose';
import { IAuthorViewModel } from '../../../shared/viewModels/index'

const Schema = mongoose.Schema;
interface IAuthorEntity extends IAuthorViewModel, mongoose.Document { }
export const AuthorSchema = new Schema({
    name: {
        type: String,
        required: 'Enter a name'
    }
});
export const AuthorRepository = mongoose.model<IAuthorEntity>('Author', AuthorSchema);