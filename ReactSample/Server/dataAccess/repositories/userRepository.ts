import * as mongoose from 'mongoose';
import { IUserViewModel } from '../../../shared/viewModels';
interface IUserEntity extends IUserViewModel, mongoose.Document { }
mongoose.model('User', new mongoose.Schema({
  email: String,
  fullName: String,
  password:String
}));

export const UserRepository = mongoose.model<IUserEntity>('User');

