import * as BooksStore from './books.store';
import * as AuthorsStore from './authors.store';
import * as LoginStore from '../pages/login/login.store';
import * as RegisterStore from '../pages/register/register.store';
import { createStore, applyMiddleware, combineReducers, ReducersMapObject } from 'redux'
import createSagaMiddleware from 'redux-saga'


// render the application
