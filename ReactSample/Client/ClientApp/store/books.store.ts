import { fetch, addTask } from 'domain-task';
import { Action, Reducer } from 'redux';
import axios from 'axios';
import { AppThunkAction } from 'ClientApp/configureStore';
import { environment } from 'ClientApp/environment/environment';
// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface BooksState {
	isBooksLoading: boolean;
	books: BookModel[];
}

export interface BookModel {
	title: string;
	authorId: string;
	_id?: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestBooksAction {
	type: 'REQUEST_BOOKS';
}

interface ReceiveBooksAction {
	type: 'RECEIVE_BOOKS';
	books: BookModel[];
}

interface CreateBooksAction {
	type: 'CREATE_BOOKS';
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestBooksAction | ReceiveBooksAction | CreateBooksAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
	requestBooks: (authorId: string): AppThunkAction<KnownAction> => (dispatch: any, getState: any) => {
		let fetchTask = axios.get<BookModel[]>(environment().apiEndpoint + `book/get/` + authorId)
			.then(data => {
				dispatch({ type: 'RECEIVE_BOOKS', books: data.data });
			});

		addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
	},
	createBook: (author: BookModel, callback: () => void): AppThunkAction<KnownAction> => (dispatch: any, getState: any) => {
		let fetchTask = axios.post(environment().apiEndpoint + `book`, author)
			.then(data => {
				dispatch({ type: 'CREATE_BOOKS' });
				callback();
			});
		addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
	}
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: BooksState = { books: [], isBooksLoading: false };

export const reducer: Reducer<BooksState> = (state: BooksState, incomingAction: Action) => {
	const action = incomingAction as KnownAction;
	switch (action.type) {
		case 'REQUEST_BOOKS':
			return {
				books: state.books,
				isBooksLoading: true
			};
		case 'RECEIVE_BOOKS':
			return {
				books: action.books,
				isBooksLoading: false
			};
	}

	return state || unloadedState;
};
