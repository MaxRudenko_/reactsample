import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import LoginPage from './pages/login/login.page';
import RegisterPage from './pages/register/register.page';
import BookPage from './pages/books/books.page';
import Home from './pages/home';
export const unauthorizedRoutes = <div>
	<Route exact path='/' component={LoginPage} />
	<Route exact path='/register' component={RegisterPage} />
</div>;

export const routes = <Layout>
	<Route exact path='/' component={Home} />
	<Route path='/books/:authorId?' component={BookPage} />
</Layout>;
