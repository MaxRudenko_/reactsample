import { Action, Reducer, Dispatch } from 'redux';
import { AuthService } from '../../services';
import { call, put, takeEvery } from 'redux-saga/effects'
import { IUserViewModel } from '../../../../shared/viewModels';
const authService = new AuthService()
export interface RegisterState {
    isLoading: boolean;
    token: string;
    user: IUserViewModel;
}
interface RegisterRequestAction {
    type: "REGISTER_REGISTER_REQUEST";
}
interface RegisterInitAction {
    type: "REGISTER_REGISTER_INIT";
    model: { email: string, password: string, fullName: string }
}
interface RegisterSuccessAction {
    type: "REGISTER_REGISTER_SUCCESS";
    token: string;
    user: IUserViewModel;
    isLoading: boolean;
}
interface RegisterFailureAction {
    type: "REGISTER_REGISTER_FAILURE";
    message: string;
}
type KnownAction = RegisterRequestAction | RegisterInitAction | RegisterSuccessAction | RegisterFailureAction;

export interface DispatchProps {
    register(model: { email: string, password: string, fullName: string }): void;
}
export const DispatchMapper = (dispatch: Dispatch<KnownAction>): DispatchProps => {
    return {
        register: (model: { email: string, password: string, fullName: string }) => {
            dispatch({ type: "REGISTER_REGISTER_REQUEST", model })
        }
    };
};

const unloadedState: RegisterState = {
    isLoading: false, token: "", user: {
        _id: "",
        fullName: "",
        email: "",
        password: ""
    }
};
export const reducer: Reducer<RegisterState> = (state: RegisterState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case "REGISTER_REGISTER_REQUEST":
            return unloadedState;
        case "REGISTER_REGISTER_INIT":
            unloadedState.isLoading = true;
            return unloadedState;
        case "REGISTER_REGISTER_SUCCESS":
            state.user = action.user;
            state.token = action.token;
            state.isLoading = action.isLoading;
            return state;
        case "REGISTER_REGISTER_FAILURE":
            return unloadedState;
    }
    return state || unloadedState;
};
function* registerSaga() {
    yield takeEvery("REGISTER_REGISTER_REQUEST", function* (action: RegisterInitAction) {
        try {
            yield put({ type: "REGISTER_REGISTER_INIT" });
            const data = yield call(authService.register, action.model);
            yield put({ type: "REGISTER_REGISTER_SUCCESS", token: data.data.token, user: data.data.user });
        } catch (e) {
            yield put({ type: "REGISTER_REGISTER_FAILURE", message: e.message });
        }
    });
}
export const registerSagaList: any[] = [registerSaga()]