import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import "./login.page.less"
import * as LoginStore from "./login.store";
import { ApplicationState } from '../../configureStore';

type LoginPageProps = LoginStore.LoginState
	& LoginStore.DispatchProps
	& RouteComponentProps<{}>;
interface State {
	username?: string;
	password?: string;
}
class LoginPage extends React.Component<LoginPageProps, State> {

	componentWillMount() {
		this.setState({
			username: "",
			password: ""
		});
	}

	componentWillReceiveProps(nextProps: LoginPageProps) {
		if (nextProps.token != "") {
			window.location.reload(true);
		}
	}

	handleChange(e: React.ChangeEvent<HTMLInputElement>) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

	public login() {
		if (!this.state.username || !this.state.password) {
			alert("Required all fields!")
			return;
		}
		this.props.login(this.state.username, this.state.password);
	}

	public render() {
		const { username, password } = this.state;
		return <div className="login-form col-md-4 col-md-offset-4">
			<h2 className="text-center">Log in</h2>
			{this.props.message ?
				<div className="alert alert-danger" role="alert">
					{this.props.message}
				</div>
				: []}
			<div className="form-group">
				<input name="username" value={username} onChange={(e) => this.handleChange(e)} type="text" className="form-control" placeholder="Username" />
			</div>
			<div className="form-group">
				<input name="password" value={password} onChange={(e) => this.handleChange(e)} type="password" className="form-control" placeholder="Password" />
			</div>
			<div className="form-group">
				<button onClick={() => this.login()} type="submit" className="btn btn-primary btn-block">Log in</button>
			</div>
			<p className="text-center"><a onClick={() => this.props.history.push("/register")}>Create an Account</a></p>
		</div>;
	}
}
let mapStateToProps = (state: ApplicationState, ownProps: any): Partial<LoginPageProps> => {
	return state.login
}
export default connect(mapStateToProps, LoginStore.DispatchMapper)(LoginPage)

