import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect, Dispatch } from 'react-redux';
import "./books.page.less"
import * as BookStore from '../../store/books.store';
import * as AuthorsStore from '../../store/authors.store';
import { bindActionCreators } from 'redux';
import BooksTableComponent from '../../components/book-table/book-table.component';
import AuthorsTableComponent from '../../components/author-table/author-table.component';
import Modal from 'react-responsive-modal';
import { ApplicationState } from 'ClientApp/configureStore';

export interface OwnProps {

}
interface StateProps {
	bookState: BookStore.BooksState,
	authorState: AuthorsStore.AuthorState
}
interface DispatchProps {
	bookActions: typeof BookStore.actionCreators;
	authorsActions: typeof AuthorsStore.actionCreators;
}
interface State {
	modalCreateAuthorIsOpen: boolean,
	modalCreateBookIsOpen: boolean,
	authorCreateModel: AuthorsStore.AuthorModel;
	bookCreateModel: BookStore.BookModel;
	selectedAuthorId: string | undefined
}
type Props = RouteComponentProps<{ authorId: string }> & StateProps & DispatchProps & OwnProps


class BooksPage extends React.Component<Props, State> {
	componentWillMount() {
		this.setState({
			modalCreateAuthorIsOpen: false,
			modalCreateBookIsOpen: false,
			authorCreateModel: {
				name: "",
				_id: ""
			},
			bookCreateModel: {
				title: "",
				authorId: ""
			}
		})
	}

	componentWillReceiveProps(nextProps: Props) {
	}

	public selectAuthor(id: string) {
		this.setState({
			selectedAuthorId: id
		})
		this.props.history.push("/books/" + id)
	}

	public openCreateBookModal() {
		this.setState({ modalCreateBookIsOpen: true })
	}

	public closeCreateBookModal() {
		this.setState({ modalCreateBookIsOpen: false })
	}

	public openCreateAuthorModal() {
		this.setState({ modalCreateAuthorIsOpen: true })
	}

	public closeCreateAuthorModal() {
		this.setState({ modalCreateAuthorIsOpen: false })
	}

	public saveAuthor() {
		this.props.authorsActions.createAuthor(this.state.authorCreateModel, () => {
			this.props.authorsActions.requestAuthors()
			this.closeCreateAuthorModal()
		})
	}

	public saveBook() {
		this.props.bookActions.createBook(this.state.bookCreateModel, () => {
			this.props.bookActions.requestBooks(this.props.match.params.authorId)
			this.closeCreateBookModal()
		})
	}

	public render() {
		return <div>
			<h1>Books</h1>
			<p>Our library</p>
			{this.renderTables()}
		</div>
	}

	public renderTables() {
		if (this.props.match.params.authorId) {
			return <div className="row">
				{this.renderHeaderContainer()}
				<div className="col-md-6">
					{/* <AuthorsTableComponent onAuthorClick={(id) => this.selectAuthor(id)} /> */}
				</div>
				<div className="col-md-6">
					{/* <BooksTableComponent authorId={this.props.match.params.authorId} /> */}
				</div>
			</div>
		} else {
			return <div>
				{this.renderHeaderContainer()}
				<div className="col-md-12">
					{/* <AuthorsTableComponent onAuthorClick={(id) => this.selectAuthor(id)} /> */}
				</div>
			</div>
		}
	}

	private renderHeaderContainer() {
		return <div className="col-md-12">
			<button onClick={(e) => this.openCreateAuthorModal()} className="btn btn-default">Create Author</button>
			{this.props.match.params.authorId ? <button onClick={(e) => this.openCreateBookModal()} className="btn btn-default pull-right">Attach new book</button> : []}
			<Modal open={this.state.modalCreateAuthorIsOpen} onClose={() => this.closeCreateAuthorModal()} center>
				<div className="col-md-12">
					<h2>Create Author</h2>
					<input value={this.state.authorCreateModel.name} onChange={(name) => this.setState({
						authorCreateModel: {
							name: name.target.value
						}
					})} placeholder="Name" className="form-control" />
				</div>
				<div className="col-md-12">
					<button className="btn btn-success pull-right" onClick={() => this.saveAuthor()}>Save</button>
				</div>
			</Modal>
			<Modal open={this.state.modalCreateBookIsOpen} onClose={() => this.closeCreateBookModal()} center>
				<div className="col-md-12">
					<h2>Attach Book</h2>
					<input value={this.state.bookCreateModel.title} onChange={(title) => this.setState({
						bookCreateModel: {
							title: title.target.value,
							authorId: this.props.match.params.authorId
						}
					})} placeholder="Title" className="form-control" />
				</div>
				<div className="col-md-12">
					<button className="btn btn-success pull-right" onClick={() => this.saveBook()}>Save</button>
				</div>
			</Modal>
		</div>
	}
}
let mapStateToProps = (state: ApplicationState, ownProps: any) : any => {
	return {
		bookState: state.book,
		authorState: state.author
	}
}
let mapDispatchToProps = (dispatch: Dispatch<any>, ownProps: any): DispatchProps => {
	return {
		bookActions: BookStore.actionCreators,
		authorsActions: AuthorsStore.actionCreators
	};
}
export default connect<ApplicationState, DispatchProps, any>
	(mapStateToProps, mapDispatchToProps)(BooksPage)

