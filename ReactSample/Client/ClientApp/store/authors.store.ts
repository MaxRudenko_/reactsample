import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import axios from 'axios';
import { environment } from 'ClientApp/environment/environment';
import { AppThunkAction } from 'ClientApp/configureStore';
// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface AuthorState {
	isAuthorsLoading: boolean;
	authors: AuthorModel[];
}

export interface AuthorModel {
	name: string;
	_id?: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestAuthorsAction {
	type: 'REQUEST_AUTHORS';
}

interface ReceiveAuthorsAction {
	type: 'RECEIVE_AUTHORS';
	authors: AuthorModel[];
}

interface CreateAuthorsAction {
	type: 'CREATE_AUTHORS';
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestAuthorsAction | ReceiveAuthorsAction | CreateAuthorsAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
	requestAuthors: (): AppThunkAction<KnownAction> => (dispatch: any, getState: any) => {
		let fetchTask = axios.get<AuthorModel[]>(environment().apiEndpoint + `author`)
			.then(data => {
				dispatch({ type: 'RECEIVE_AUTHORS', authors: data.data });
			});

		addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
	},
	createAuthor: (author: AuthorModel,callback:()=>void): AppThunkAction<KnownAction> => (dispatch: any, getState: any) => {
		let fetchTask = axios.post(environment().apiEndpoint + `author`, author)
			.then(data => {
				dispatch({ type: 'CREATE_AUTHORS', authors: data });
				callback();
			});
		addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
	}
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: AuthorState = { authors: [], isAuthorsLoading: false };

export const reducer: Reducer<AuthorState> = (state: AuthorState, incomingAction: Action) => {
	const action = incomingAction as KnownAction;
	switch (action.type) {
		case 'REQUEST_AUTHORS':
			return {
				authors: state.authors,
				isAuthorsLoading: true
			};
		case 'RECEIVE_AUTHORS':
			return {
				authors: action.authors,
				isAuthorsLoading: false
			};
		case 'CREATE_AUTHORS':
			return state;
		default:
			// The following line guarantees that every action in the KnownAction union has been covered by a case above
			const exhaustiveCheck: never = action;
	}

	return state || unloadedState;
};
