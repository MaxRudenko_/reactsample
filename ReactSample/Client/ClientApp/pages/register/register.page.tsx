import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import 'regenerator-runtime/runtime'
import { connect } from "react-redux";
import "./register.page.less"
import { ApplicationState } from "../../configureStore";
import * as RegisterStore from "./register.store"

type RegisterPageProps = RegisterStore.RegisterState
	& RegisterStore.DispatchProps
	& RouteComponentProps<{}>;

interface State {
	username: string;
	password: string;
	fullName: string;
}
class RegisterPage extends React.Component<RegisterPageProps, State> {
	componentWillMount() {
		this.setState({
			username: "",
			password: "",
			fullName: ""
		})
	}

	componentWillReceiveProps(nextProps: RegisterPageProps) {
		debugger;
		if (nextProps.token != "") {
			this.props.history.push("/");
		}
	}

	public register() {
		this.props.register({
			email: this.state.username,
			password: this.state.password,
			fullName: this.state.fullName
		});
	}

	public render() {
		return <div className="login-form col-md-4 col-md-offset-4">
			<h2 className="text-center">Registration</h2>
			<div className="form-group">
				<input value={this.state.fullName} onChange={(value) => this.setState({
					fullName: value.target.value
				})} type="text" className="form-control" placeholder="Full Name" />
			</div>
			<div className="form-group">
				<input value={this.state.username} onChange={(value) => this.setState({
					username: value.target.value
				})} type="text" className="form-control" placeholder="Username" />
			</div>
			<div className="form-group">
				<input value={this.state.password} onChange={(value) => this.setState({
					password: value.target.value
				})} type="password" className="form-control" placeholder="Password" />
			</div>
			<div className="form-group">
				<button onClick={() => this.register()} type="submit" className="btn btn-primary btn-block">Register</button>
			</div>
			<p className="text-center"><a onClick={() => this.props.history.push("/")}>Login with existing account</a></p>
		</div>;
	}
}

const mapStateToProps = (state: ApplicationState): Partial<RegisterPageProps> => {
	return state.register
}

export default connect(mapStateToProps, RegisterStore.DispatchMapper)(RegisterPage as any)
