import * as mongoose from 'mongoose';
import { IBookViewModel } from '../../../shared/viewModels';

const Schema = mongoose.Schema;
interface IBookEntity extends IBookViewModel, mongoose.Document { }
export const BookSchema = new Schema({
    title: {
        type: String,
        required: 'Enter a title'
    },
    authorId: {
        type: String,
        required: 'Select author please'
    }
});
export const BookRepository = mongoose.model<IBookEntity>('Book', BookSchema);