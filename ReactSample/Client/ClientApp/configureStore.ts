import "regenerator-runtime/runtime";
import createSagaMiddleware from "redux-saga"
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { History } from 'history';
import * as BooksStore from './store/books.store';
import * as AuthorsStore from './store/authors.store';
import * as LoginStore from './pages/login/login.store';
import * as RegisterStore from './pages/register/register.store';
import { all } from 'redux-saga/effects';
export interface ApplicationState {
    book: BooksStore.BooksState;
    author: AuthorsStore.AuthorState;
    register: RegisterStore.RegisterState,
    login: LoginStore.LoginState
}
export const reducers = {
    books: BooksStore.reducer,
    authors: AuthorsStore.reducer,
    register: RegisterStore.reducer,
    login: LoginStore.reducer
};
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}

export default function configureStore(history: History) {
    const composeEnhancers = (window as any)["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] || compose;
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        combineReducers<ApplicationState>(reducers),
        composeEnhancers(applyMiddleware(sagaMiddleware, routerMiddleware(history)))
    );
    const effects: any[] = [...RegisterStore.registerSagaList,... LoginStore.loginSagaList];
    
    sagaMiddleware.run(function* () {
        yield all(effects);
    });
    return store;
}

